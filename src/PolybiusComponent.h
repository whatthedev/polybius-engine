#pragma once
#define POLYBIUSCOMPONENT_H
#include "InvalidOperationException.h"

struct PolybiusComponent
{
public:
	PolybiusComponent();
	~PolybiusComponent();
	unsigned long long GetEntityId();
	void SetEntityId(unsigned long long entityId);
private:
	unsigned long long _entityId;
	bool _isAssigned;
};

