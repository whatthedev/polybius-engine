#include "EcsManager.h"

EcsManager* EcsManager::_instance;

EcsManager::EcsManager()
{
}

void EcsManager::AddSystems(std::vector<IPolybiusSystem*> systems)
{
	for (auto &system : systems)
	{
		_systems.push_back(std::unique_ptr<IPolybiusSystem>(system));
	}
}

void EcsManager::CreateNewEntity()
{
	bool foundValidId = false;
	unsigned long long newId;
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<unsigned long long> dis(0, ULLONG_MAX);

	while (!foundValidId)
	{
		newId = dis(gen);
		while (true)
		{
			bool exists = false;
			for (int i = 0; i < _entities.size(); i++)
			{
				if (_entities[i]->GetId() == newId)
				{
					exists = true;
					break;
				}
			}
			if (!exists)
			{
				PolybiusEntity newEnt(newId);
				_entities.push_back(std::make_unique<PolybiusEntity>(newEnt));
			}
		}
	}
}

void EcsManager::DestroyEntity(unsigned long long id)
{
	for (int i = 0; i < _entities.size(); i++)
	{
		if (_entities[i]->GetId() == id)
		{
			_entities.erase(_entities.begin() + i, _entities.begin() + i);
			return;
		}
	}
}

PolybiusEntity* EcsManager::GetEntity(unsigned long long id)
{
	for (int i = 0; i < _entities.size(); i++)
	{
		if (_entities[i]->GetId() == id)
		{
			return _entities[i].get();
		}
	}
	return nullptr;
}

void EcsManager::RegisterComponent(PolybiusComponent* component, unsigned long long entityId)
{
	PolybiusEntity* entity = GetEntity(entityId);

	if (entity == nullptr) throw InvalidOperationException("Unable to add component to Entity! Specified Entity does not exist within the ECS!");

	component->SetEntityId(entityId);
	_componentBag.push_back(std::unique_ptr<PolybiusComponent>(component));

}

std::vector<PolybiusComponent*> EcsManager::GetComponents(std::vector<std::type_info> types)
{
	std::vector<PolybiusComponent*> returnVector;

	for (int i = 0; i < types.size(); i++)
	{
		for (int j = 0; j < _componentBag.size(); j++)
		{
			if (types[i] == typeid(_componentBag[j]))
			{
				returnVector.push_back(_componentBag[j].get());
			}
		}
	}
	return returnVector;
}

EcsManager* EcsManager::Instance()
{
	if (_instance == nullptr)
	{
		_instance = new EcsManager();
	}

	return _instance;
}

void EcsManager::FrameUpdateAllSystems(float deltaTime)
{
	for (auto &system : _systems)
	{
		system->FrameUpdate(deltaTime);
	}
}


