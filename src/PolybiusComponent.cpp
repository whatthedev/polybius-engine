#include "PolybiusComponent.h"



PolybiusComponent::PolybiusComponent()
{
}


PolybiusComponent::~PolybiusComponent()
{
}

void PolybiusComponent::SetEntityId(unsigned long long entityId)
{
	if (!_isAssigned)
	{
		_entityId = entityId;
		_isAssigned = true;
	}
	else
	{
		throw InvalidOperationException("Entity ID was trying to be assigned when it already has an entity!");
	}
}

unsigned long long PolybiusComponent::GetEntityId()
{
	return _entityId;
}
