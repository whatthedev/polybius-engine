#include "raylib.h"
#include "polybiuseditorutils.h"

static int screenWidth = 800;
static int screenHeight = 450;

static int _tick;
static float _deltaTime;

static Camera _camera;
static Vector3 _cubePosition;
static Vector3 _cube2Position;
static Vector3 _cube3Position;

static void InitEditor(void);      // Initialize Editor
static void UpdateEditor(void);    // Update Editor (one frame)
static void DrawEditor(void);      // Draw Editor (one frame)
static void UnloadEditor(void);    // Unload Editor
static void UpdateDrawFrame(void); // Update and Draw (one frame)

int main(void)
{
    InitEditor();

    SetTargetFPS(60);

    // Main Editor loop
    while (!WindowShouldClose()) // Detect window close button or ESC key
    {
        UpdateDrawFrame();
    }

    UnloadEditor(); // Unload loaded data (textures, sounds, models...)

    CloseWindow(); // Close window and OpenGL context
}

void InitEditor(void)
{
    // Initialize Editor
    InitWindow(screenWidth, screenHeight, "Polybius Engine");
    SetConfigFlags(FLAG_MSAA_4X_HINT);

    _camera.fovy = 60.0f;
    _camera.position = (Vector3){10, 2, 0};
    _camera.target = (Vector3){0, 0, 0};
    _camera.up = (Vector3){0, 1, 0};

    SetCameraMode(_camera, CAMERA_FREE);
    SetCameraPanControl(MOUSE_RIGHT_BUTTON);
    SetCameraAltControl(KEY_SPACE);

    _cubePosition = (Vector3){0, 1, 0};
    _cube2Position = (Vector3){0, 1, 2};
    _cube3Position = (Vector3){0, 3, 2};

    _tick = 0;
    _deltaTime = 0;
}

void UpdateEditor(void)
{
    // Editor loop
    _tick++;
    _deltaTime = 1.0f / GetFPS();

    UpdateCamera(&_camera);

    if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
    {
        MoveCubeRandom(&_cubePosition);
    }
}

void DrawEditor(void)
{
    // Drawing loop
    BeginDrawing();

    ClearBackground(DARKGRAY);

    DrawEditorGUI();

    Begin3dMode(_camera);

    DrawEditorUtils();

    DrawCube(_cubePosition, 2, 2, 2, ORANGE);
    DrawCubeWires(_cubePosition, 2, 2, 2, BLUE);
    DrawCube(_cube2Position, 2, 2, 2, GREEN);
    DrawCubeWires(_cube2Position, 2, 2, 2, BLUE);
    DrawCube(_cube3Position, 2, 2, 2, PINK);
    DrawCubeWires(_cube3Position, 2, 2, 2, BLUE);

    End3dMode();

    DrawFPS(0, 0);

    EndDrawing();
}

void UnloadEditor(void)
{
    // Unload stuff
}

void UpdateDrawFrame(void)
{
    UpdateEditor();
    DrawEditor();
}