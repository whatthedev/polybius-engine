#pragma once
#ifndef _VECTOR_
#include <vector>
#endif // !_VECTOR_
#ifndef POLYBIUSENTITY_H
#include "PolybiusEntity.h"
#endif // !POLYBIUSENTITY_H
#include "IPolybiusSystem.h"
#ifndef POLYBIUSSYSTEM_H
#include "PolybiusSystem.h"
#endif // !POLYBIUSENTITY_H
#ifndef _RANDOM_
#include <random>
#endif // !
#ifndef _TYPEINFO_
#include <typeinfo>
#endif // !_TYPEINFO_
#include <memory>

class EcsManager
{
public:
	void AddSystems(std::vector<IPolybiusSystem*> systems);
	void CreateNewEntity();
	PolybiusEntity* GetEntity(unsigned long long id);
	void DestroyEntity(unsigned long long id);
	void RegisterComponent(PolybiusComponent* component, unsigned long long entityId);
	std::vector<PolybiusComponent*> GetComponents(std::vector<std::type_info> types);
	static EcsManager* Instance();
	void FrameUpdateAllSystems(float deltaTime);
private:
	static EcsManager* _instance;
	std::vector<std::unique_ptr<PolybiusEntity>> _entities;
	std::vector<std::unique_ptr<IPolybiusSystem>> _systems;
	std::vector<std::unique_ptr<PolybiusComponent>> _componentBag;
	EcsManager();
};

