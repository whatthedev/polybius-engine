#pragma once
#include "EcsManager.h"
#include "IPolybiusSystem.h"

template <typename T>
class PolybiusSystem : IPolybiusSystem
{
public:
	PolybiusSystem();
	void Add(T& component);
	void Remove(T& component);

private:
	std::vector<T*>& _components;
	virtual void ComponentStart(T& component) = 0;
};

