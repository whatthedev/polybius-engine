#include "PolybiusSystem.h"

template <typename T>
PolybiusSystem<T>::PolybiusSystem()
{
	_components = new std::vector<T>();
}

template<typename T>
void PolybiusSystem<T>::Add(T & component)
{
	_components.push_back(component);
}

template<typename T>
void PolybiusSystem<T>::Remove(T & component)
{
	_components.erase(std::remove(_components.begin(), _components.end(), component), _components.end);
}

