#define POLYBUISEDITORUTILS_H

#ifndef RAYLIB_H
#include "raylib.h"
#endif

#ifndef RAYGUI_H
#define RAYGUI_STATIC
#define RAYGUI_STYLE_SAVE_LOAD
#define RAYGUI_STYLE_DEFAULT_DARK
#include "raygui.h"
#endif

Rectangle panel = { 800 / 2 - 130, 450 / 2 - 110, 260, 230 };

void DrawEditorGrid()
{
	DrawGrid(20, 1);

	DrawLine3D(Vector3Zero(), (Vector3) { 10, 0, 0 }, RED);
	DrawLine3D(Vector3Zero(), (Vector3) { 0, 10, 0 }, GREEN);
	DrawLine3D(Vector3Zero(), (Vector3) { 0, 0, 10 }, BLUE);
}

void DrawEditorGUI()
{
	// TODO: Find out why it thinks the functions are not defined :[
	/*GuiDisable();
	GuiWindowBox(panel, "This is a GUI");
	GuiEnable();
	GuiLabel((Rectangle) { 30, 30, 100, 20 }, "test label");*/
}

void DrawEditorUtils()
{
	DrawEditorGrid();
}

//temp
void MoveCubeRandom(Vector3* pos)
{
	pos->x = GetRandomValue(-10, 10);
	pos->y = GetRandomValue(-2, 5);
	pos->z = GetRandomValue(-10, 10);
}