#pragma once
#include <stdexcept>
class InvalidOperationException : public std::exception
{
public:
	InvalidOperationException(std::string message);
	~InvalidOperationException();
	const std::string message() const throw()
	{
		return _message;
	}

private:
	std::string _message;
};

