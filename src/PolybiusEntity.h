#pragma once
#ifndef _VECTOR_
#include <vector>
#endif // !_VECTOR_
#ifndef POLYBIUSCOMPONENT_H
#include "PolybiusComponent.h"
#endif // !POLYBIUSCOMPONENT_H
#define POLYBIUSENTITY_H

struct PolybiusEntity
{
public:
	unsigned long long GetId();
	PolybiusEntity(unsigned long long id);

private:
	unsigned long long _id;
};

