# The Polybius Engine

The Polybius Engine is a small game engine built on [raylib](https://github.com/raysan5/raylib). You can follow along with our progress on Wednesdays when we livestream [on Kellen's channel](https://twitch.tv/Pothead_Pete) and [Matt's channel](https://twitch.tv/RubyNovaWTD).

## Announcement

The Polybius Engine is currently on halt. Since we have last worked on this project, raylib has released 2.0 and we have made multiple decisions on how to take the engine further.
As a result of this, we will essentially have to start from scratch, but this time we will have a solid foundation from the start.

## To Implement

- [x] ECS
- [ ] Level Editor
- [ ] Level Saving/Loading
- [ ] Runtime asset loading (data oriented design)
- [ ] Core loop

## Built With

- [raylib](https://github.com/raysan5/raylib)
- [raygui](https://github.com/raysan5/raygui)

## Contact

You can contact us on the [What The Dev Discord](http://discord.whathedev.net)