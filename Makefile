.PHONY: all clean

PROJECT_NAME ?= memes
RELEASE_MODE ?= DEBUG

RAYLIB_VERSION ?= 1.9.7
RAYLIB_API_VERSION ?= 1
RAYLIB_PATH ?= ..

# Default options
PLATFORM ?= PLATFORM_DESKTOP

# Linux only??
DESTDIR ?= /usr/local
RAYLIB_INSTALL_PATH ?= $(DESTDIR)/lib
RAYLIB_H_INSTALL_PATH ?= $(DESTDIR)/include

RAYLIB_LIBTYPE ?= STATIC
RAYLIB_BUILD_MODE ?= $(RELEASE_MODE)
USE_EXTERNAL_GLFW ?= FALSE

# Wayland or X11
USE_WAYLAND_DISPLAY ?= FALSE


# If PLATFORM_DESKTOP, determine PLATFORM_OS
ifeq ($(PLATFORM), PLATFORM_DESKTOP)
	ifeq ($(OS), Windows_NT)
		PLATFORM_OS=WINDOWS
	else
		UNAMEOS=$(shell uname)
		ifeq ($(UNAMEOS), Linux)
			PLATFORM_OS=LINUX
		endif
		ifeq ($(UNAMEOS), Darwin)
			PLATFORM_OS=DARWIN
		endif
	endif
endif
ifeq ($(PLATFORM), PLATFORM_RPI)
	UNAMEOS=$(shell uname)
	ifeq ($(UNAMEOS), Linux)
		PLATFORM_OS=LINUX
	endif
endif

# RAYLIB_PATH adjustment for different platforms
ifeq ($(PLATFORM), PLATFORM_DESKTOP)
	ifeq ($(PLATFORM_OS), LINUX)
		RAYLIB_PREFIX ?= ..
		RAYLIB_PATH = $(realpath $(RAYLIB_PREFIX))
	endif
endif
# is this needed??
ifeq ($(PLATFORM), PLATFORM_RPI)
	RAYLIB_PATH ?= /home/pi/raylib
endif

# Define raylib release directory for the compiled library
# TODO: does this change in DEBUG mode??
ifeq ($(PLATFORM), PLATFORM_DESKTOP)
	ifeq ($(PLATFORM_OS), WINDOWS)
		RAYLIB_RELEASE_PATH = $(RAYLIB_PATH)/release/libs/win32/mingw32 #TODO: build without Mingw32?
	endif
	ifeq ($(PLATFORM_OS), LINUX)
		RAYLIB_RELEASE_PATH = $(RAYLIB_PATH)/release/libs/linux
	endif
	ifeq ($(PLATFORM_OS), DARWIN)
		RAYLIB_RELEASE_PATH = $(RAYLIB_PATH)/release/libs/osx
	endif
endif
ifeq ($(PLATFORM), PLATFORM_RPI)
	RAYLIB_RELEASE_PATH = $(RAYLIB_PATH)/release/libs/rpi
endif

# EXAMPLE_RUNTIME_PATH ?= $(RAYLIB_RELEASE_PATH)
CC = g++

ifeq ($(PLATFORM), PLATFORM_DESKTOP)
	ifeq ($(PLATFORM_OS), DARWIN)
		CC = clang
	endif
endif
ifeq ($(PLATFORM), PLATFORM_RPI)
	ifeq ($(USE_CROSS_COMPILER), TRUE)
		#CC = armv6j-hardfloat-linux-gnueabi-gcc
		CC = $(RPI_TOOLCHAIN)/bin/arm-linux-gnuabihf-gcc
	endif
endif

# TODO: remove need for Mingw32
MAKE = make

ifeq ($(PLATFORM), PLATFORM_DESKTOP)
	ifeq ($(PLATFORM_OS), WINDOWS)
		MAKE = mingw32-make
	endif
endif

# Define compiler flags:
#  -O1                  defines optimization level
#  -g                   enable debugging
#  -s                   strip unnecessary data from build
#  -Wall                turns on most, but not all, compiler warnings
#  -std=c99             defines C language mode (standard C from 1999 revision)
#  -std=gnu99           defines C language mode (GNU C from 1999 revision)
#  -Wno-missing-braces  ignore invalid warning (GCC bug 53119)
#  -D_DEFAULT_SOURCE    use with -std=c99 on Linux and PLATFORM_WEB, required for timespec
CFLAGS += -O1 -s -Wall -D_DEFAULT_SOURCE -Wno-missing-braces #-std=c99

ifeq ($(PLATFORM), PLATFORM_DESKTOP)
	ifeq ($(PLATFORM_OS), LINUX)
		ifeq ($(RELEASE_MODE), DEBUG)
			CFLAGS += -g
		endif
		ifeq ($(RAYLIB_LIBTYPE), STATIC)
			CFLAGS += -no-pie -D_DEFAULT_SOURCE
		endif
		ifeq ($(RAYLIB_LIBTYPE), SHARED)
			CFLAGS += -Wl,#-rpath,$(EXAMPLE_RUNTIME_PATH)
		endif
	endif
endif

INCLUDE_PATHS = -I. -I$(RAYLIB_PATH)/release/include -I$(RAYLIB_PATH)/src -I$(RAYLIB_PATH)/src/external

ifeq ($(PLATFORM), PLATFORM_RPI)
	INCLUDE_PATHS += -I/opt/vc/include
	INCLUDE_PATHS += -I/opt/vc/include/interface/vmcs_host/linux
	INCLUDE_PATHS += -I/opt/vc/include/interface/vcos/pthreads
endif
ifeq ($(PLATFORM), PLATFORM_DESKTOP)
	ifeq ($(PLATFORM_OS), LINUX)
		INCLUDE_PATHS = -I$(RAYLIB_H_INSTALL_PATH) -isystem. -isystem$(RAYLIB_PATH)/src -isystem$(RAYLIB_PATH)/release/include -isystem$(RAYLIB_PATH)/src/external
	endif
endif


LDFLAGS = -L. -L$(RAYLIB_RELEASE_PATH) -L$(RAYLIB_PLATH)/src

ifeq ($(PLATFORM), PLATFORM_DESKTOP)
	ifeq ($(PLATFORM_OS), LINUX)
		LDFLAGS = -L. -L$(RAYLIB_INSTALL_PATH) -L$(RAYLIB_RELEASE_PATH) -L$(RAYLIB_PATH)/src
	endif
endif

ifeq ($(PLATFORM), PLATFORM_RPI)
	LDFLAGS += -L/opt/vc/lib
endif

# Define libraries required in linking
ifeq ($(PLATFORM), PLATFORM_DESKTOP)
	ifeq ($(PLATFORM_OS), WINDOWS)
		LDLIBS = -lraylib -lopengl32 -lgdi32
		#LDLIBS += -static -lpthread
	endif
	ifeq ($(PLATFORM_OS), LINUX)
		LDLIBS = -lraylib -lGL -lm -lpthread -ldl -lrt
		ifneq ($(USE_WAYLAND_DISPLAY), TRUE) # TODO: should this be conditional or not?
			LDLIBS += -lX11
		else
			LDLIBS += -lwayland-client -lwayland-cursor -lwayland-egl -lxkbcommon
		endif
		ifeq ($(RAYLIB_LIBTYPE), SHARED)
			LDLIBS += lc
		endif
	endif
	ifeq ($(PLATFORM_OS), DARWIN)
		LDLIBS = -lraylib -framework OpenGL -framework OpenAL -framework Cocoa
	endif
	ifeq ($(USE_EXTERNAL_GLFW), TRUE)
		# requires libglfw3-dev
		LDLIBS += -lglfw
	endif
endif
ifeq ($(PLATFORM), PLATFORM_RPI)
	LDLIBS = -lraylib -lbrcmGLESv2 -lbrcmEGL -lpthread -lrt -lm -lbcm_host -ldl
endif

OUT_DIR = bin
# Define all object files required
SRC_DIR = src

all:
	$(CC) $(SRC_DIR)/* $(CFLAGS) $(INCLUDE_PATHS) $(LDFLAGS) $(LDLIBS) -D$(PLATFORM) -o $(OUT_DIR)/$(PROJECT_NAME)

clean:
	@rm -rf  $(OUTPUT_DEST)/*